﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTool.EzCanvas
{
    public partial class BaseCanvas : MonoBehaviour //TransistionBase
    {
        [DisallowMultipleComponent]
        [RequireComponent(typeof(BaseCanvas))]
        public abstract class Transition : MonoBehaviour
        {
            private BaseCanvas canvas = null;
            public BaseCanvas Canvas => canvas;

            protected virtual void Awake()
            {
                canvas = GetComponent<BaseCanvas>();
                canvas.transitionComponent = this;
            }
            protected virtual void OnDestroy()
            {
                if(canvas != null && canvas.transitionComponent == this)
                    canvas.transitionComponent = null;
            }

            protected Action onFinishTransition = null;

            internal void baseStartShow(Action onFinishAction)
            {
                onFinishTransition = onFinishAction;
                StartShowing();
            }
            internal void baseStartHide(Action onFinishAction)
            {
                onFinishTransition = onFinishAction;
                StartHiding();
            }
            protected void baseFinishTransition()
            {
                onFinishTransition?.Invoke();
                onFinishTransition = null;
            }
            /// <summary>
            /// Base transition start showing. Remember to Invoke onFinishShowingAction after
            /// </summary>
            public abstract void StartShowing();
            /// <summary>
            /// Base transition start hiding. Remember to Invoke onFinishShowingAction after
            /// </summary>
            public abstract void StartHiding();
        }
    }
}