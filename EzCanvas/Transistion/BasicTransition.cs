﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTool.EzCanvas
{
    public class BasicTransition : BaseCanvas.Transition
    {
        public float CurrentValue { get; private set; }
        public bool IsActive
            => CurrentValue > 0f && CurrentValue < 1f;

        [SerializeField, Range(0f, 1f)]
        private float maxDur = 1f;
        [SerializeField]
        private FadeInternalTransistion fadeAnimation = new FadeInternalTransistion(true);
        [SerializeField]
        private SizeInternalTransistion sizeAnimation = new SizeInternalTransistion(false);
        [SerializeField]
        private SlideInternalTransistion slideAnimation = new SlideInternalTransistion(false);

        Coroutine activeCoroutine { get; set; }

        public override void StartShowing()
        {
            if (activeCoroutine != null)
            {
                RefreshCurrentValue();
                StopCoroutine(activeCoroutine);
            }
            activeCoroutine = StartCoroutine(_TransIn(maxDur));
        }
        public override void StartHiding()
        {
            if (activeCoroutine != null)
            {
                RefreshCurrentValue();
                StopCoroutine(activeCoroutine);
            }
            activeCoroutine = StartCoroutine(_TransOut(maxDur));
        }
        protected override void Awake()
        {
            base.Awake();
            fadeAnimation.Setup(Canvas);
            fadeAnimation.IFirstLoad();
            sizeAnimation.Setup(Canvas);
            sizeAnimation.IFirstLoad();
            slideAnimation.Setup(Canvas);
            slideAnimation.IFirstLoad();
        }

        private void RefreshCurrentValue()
        {
            if (Canvas.OnTransition == false)
                CurrentValue = Canvas.Enable ? 0 : 1;
        }
        private IEnumerator _TransIn(float maxDur)
        {
            float _timeTick = CurrentValue * maxDur;
            IUpdate(_timeTick / maxDur);
            while (true)
            {
                //Debug.Log(_timeTick);
                yield return null;
                _timeTick += Time.deltaTime;
                if (_timeTick >= maxDur)
                    break;
                CurrentValue = _timeTick / maxDur;
                IUpdate(CurrentValue);
            }
            CurrentValue = 1;
            IUpdate(CurrentValue);
            baseFinishTransition();
             activeCoroutine = null;
        }
        
        private IEnumerator _TransOut(float maxDur)
        {
            float _timeTick = CurrentValue * maxDur;
            IUpdate(_timeTick / maxDur);
            while (true)
            {
                //Debug.Log(_timeTick);
                yield return null;
                _timeTick -= Time.deltaTime;
                if (_timeTick <= 0)
                    break;
                CurrentValue = _timeTick / maxDur;
                IUpdate(CurrentValue);
            }
            CurrentValue = 0;
            IUpdate(CurrentValue);
            baseFinishTransition();
            activeCoroutine = null;
        }

        private void IUpdate(float currentValue)
        {
            fadeAnimation.SafeUpdate(currentValue);
            sizeAnimation.SafeUpdate(currentValue);
            slideAnimation.SafeUpdate(currentValue);
        }

        [System.Serializable]
        public class FadeInternalTransistion : InternalTransistionControllerBase
        {
            public FadeInternalTransistion(bool isEnabled) : base(isEnabled) { }

            public override void IFirstLoad()
            {

            }

            protected override void IUpdate(float currentValue)
            {
                Canvas.CanvasGroup.alpha = currentValue * Canvas.ActiveAlpha;
            }
        }
        [System.Serializable]
        public class SlideInternalTransistion : InternalTransistionControllerBase
        {
            [SerializeField]
            private Vector2 anchoredPosition = new Vector2(0, -250);

            private Vector2 calculatedAnchoredPosition { get; set; }
            private Vector2 firstAnchoredPosition = new Vector2(0, 0);

            public SlideInternalTransistion(bool isEnabled) : base(isEnabled) { }

            protected override void IUpdate(float currentValue)
            {
                Canvas.RectTransform.anchoredPosition = Vector2.Lerp(calculatedAnchoredPosition, firstAnchoredPosition, currentValue);
            }

            public override void IFirstLoad()
            {
                firstAnchoredPosition = Canvas.RectTransform.anchoredPosition;
                calculatedAnchoredPosition = calculatedAnchoredPosition + anchoredPosition;
            }
        }
        [System.Serializable]
        public class SizeInternalTransistion : InternalTransistionControllerBase
        {
            [SerializeField]
            private Vector2 inactiveSize = new Vector2(0f, 0f);

            private Vector2 firstSize;

            public SizeInternalTransistion(bool isEnabled) : base(isEnabled) { }

            protected override void IUpdate(float currentValue)
            {
                Canvas.RectTransform.localScale = Vector2.Lerp(inactiveSize, firstSize, currentValue);
            }
            public override void IFirstLoad()
            {
                firstSize = Canvas.RectTransform.localScale;
            }
        }

        [System.Serializable]
        public abstract class InternalTransistionControllerBase
        {
            [SerializeField]
            protected bool isEnabled = false;
            protected BaseCanvas Canvas  { get; set; }

            public InternalTransistionControllerBase (bool isEnabled)
            {
                this.isEnabled = isEnabled;
            }

            public void Setup(BaseCanvas Canvas)
            {
                this.Canvas = Canvas;
            }

            public void SafeUpdate(float currentValue)
            {
                if (isEnabled)
                    IUpdate(currentValue);
            }

            protected abstract void IUpdate(float currentValue);
            public abstract void IFirstLoad();
        }
    }
}