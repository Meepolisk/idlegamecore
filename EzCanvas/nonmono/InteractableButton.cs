﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;

namespace RTool.EzCanvas
{
    [System.Serializable]
    internal class InteractableButton
    {
        [SerializeField]
        internal string message = "";
        [SerializeField]
        internal Button button = null;

        internal void Setup (string message, Button button, UnityAction unityAction)
        {
            if (string.IsNullOrEmpty(message))
                throw new System.Exception("String cannot be null");
            this.message = message;
            this.button = button ?? throw new System.Exception("Button cannot be null");
            button.onClick.AddListener(unityAction ?? throw new System.Exception("Action cannot be null"));
        }
        internal void Setup (InteractablePopup interactablePopup)
        {
            if (string.IsNullOrEmpty(message))
            {
                Debug.LogWarningFormat(interactablePopup.gameObject, "An {0} in {1} was trying to setup a null or empty key. This is not allowed", nameof(InteractableButton), interactablePopup.gameObject.name);
                return;
            }

            button.onClick.AddListener(() => interactablePopup.OnConfirmPopupExecuse(message));
        }
#if UNITY_EDITOR
        [CustomPropertyDrawer(typeof(InteractableButton))]
        private class InteractableButtonCPD : PropertyDrawer
        {
            public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                EditorGUI.BeginProperty(position, label, property);
                position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

                var indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = 0;

                // Calculate rects
                var rect1 = new Rect(position.x, position.y, (position.width / 2) - 5, position.height);
                var rect2 = new Rect(position.x + (position.width / 2) - 5, position.y, 10, position.height);
                var rect3 = new Rect(position.x + (position.width / 2) + 5, position.y, (position.width / 2) - 5, position.height);

                EditorGUI.PropertyField(rect1, property.FindPropertyRelative(nameof(message)), GUIContent.none);
                GUI.Label(rect2, "/");
                EditorGUI.PropertyField(rect3, property.FindPropertyRelative(nameof(button)), GUIContent.none);

                // Set indent back to what it was
                EditorGUI.indentLevel = indent;

                EditorGUI.EndProperty();
            }
        }
#endif
    }
}
