﻿using UnityEngine;

namespace RTool.EzCanvas
{
    [RequireComponent(typeof(CanvasGroup), typeof(RectTransform))]
    public partial class BaseCanvas : MonoBehaviour
    {
        #region CanvasGroup ref
        [SerializeField, HideInInspector]
        protected CanvasGroup canvasGroup;
        public CanvasGroup CanvasGroup => canvasGroup;
        
        public RectTransform RectTransform => transform as RectTransform;

        /// <summary>
        /// Get/Set the real Alpha of controlled CanvasGroup. You shouldn't use this one.
        /// </summary>
        public float Alpha
        {
            get => canvasGroup.alpha;
            set => canvasGroup.alpha = value;
        }
        /// <summary>
        /// Get/Set the real Visibility of controlled CanvasGroup. You shouldn't use this one.
        /// </summary>
        public bool Visible
        {
            get => canvasGroup.alpha > 0;
            set => canvasGroup.alpha = value ? activeAlpha : 0;
        }
        /// <summary>
        /// Get/Set the real Interactable of controlled CanvasGroup. You shouldn't use this one.
        /// </summary>
        public bool Interactable
        {
            get => canvasGroup.interactable;
            set => canvasGroup.interactable = value;
        }
        internal void RefreshInteractable(bool isOn) => canvasGroup.interactable = isOn ? activeInteract : false;
        public void RefreshInteractable() => RefreshInteractable(m_Enable);

        /// <summary>
        /// Get/Set the real BlocksRaycasts of controlled CanvasGroup. You shouldn't use this one.
        /// </summary>
        public bool BlocksRaycasts
        {
            get => canvasGroup.blocksRaycasts;
            set => canvasGroup.blocksRaycasts = value;
        }
        internal void RefreshBlocksRaycasts(bool isOn) => canvasGroup.blocksRaycasts = isOn ? activeRaycast : false;
        public void RefreshBlocksRaycasts() => RefreshBlocksRaycasts(m_Enable);
        public bool IgnoreParent
        {
            get => canvasGroup.ignoreParentGroups;
            set => canvasGroup.ignoreParentGroups = value;
        }
        #endregion

        #region Transition
        //public Transition transitionComponent = null;
        public Transition transitionComponent { private set; get;}
        public bool OnTransition { private set; get; }
        #endregion

        [SerializeField, HideInInspector]
        private float activeAlpha = 1f;
        public float ActiveAlpha
        {
            get => activeAlpha;
            set
            {
                if (activeAlpha == value)
                    return;
                activeAlpha = value;
                if (m_Enable)
                    Visible = true;
            }
        }

        [SerializeField, HideInInspector]
        private bool activeInteract = true;
        public bool ActiveInteract
        {
            get => activeInteract;
            set
            {
                if (activeInteract == value)
                    return;
                activeInteract = value;
                if (m_Enable)
                    RefreshInteractable();
            }
        }
        
        [SerializeField, HideInInspector]
        private bool activeRaycast = true;
        public bool ActiveRaycast
        {
            get => activeInteract;
            set
            {
                if (activeRaycast == value)
                    return;
                activeRaycast = value;
                if (m_Enable)
                    RefreshBlocksRaycasts();
            }
        }

        [SerializeField, HideInInspector]
        protected bool m_Enable = true;
        public bool Enable => m_Enable;

        public void SetEnable(bool value, bool ignoreTransition = false)
        {
            if (m_Enable == value)
                return;

            //Debug.LogFormat("{0} is {1}: {2} TRANSITION", gameObject.name, value ? "Enable" : "Disable", ignoreTransition == true? "IGNORE" : "ALLOW");
            if (transitionComponent != null && ignoreTransition == false)
            {
                if (value == true)
                {
                    Visible = true;
                    RefreshBlocksRaycasts(true);
                    transitionComponent.baseStartShow(() =>
                    {
                        RefreshInteractable(true);
                        OnShowFinished();
                    });
                    OnShowStart();
                }
                else
                {
                    RefreshInteractable(false);
                    transitionComponent.baseStartHide(() =>
                    {
                        Visible = false;
                        RefreshBlocksRaycasts(false);
                        OnHideFinished();
                    });
                    OnShowStart();
                }
            }
            else
            {
                Visible = value;
                RefreshInteractable(value);
                RefreshBlocksRaycasts(value);
                if (value)
                {
                    OnShowStart();
                    OnShowFinished();
                }
                else
                {
                    OnHideStart();
                    OnHideFinished();
                }
            }
            m_Enable = value;
        }

        public delegate void BaseCanvasDelegate(BaseCanvas baseCanvas);
        //public event BaseCanvasDelegate onStateChanged;
        public event BaseCanvasDelegate onShowStart;
        protected virtual void OnShowStart()
        {
            onShowStart?.Invoke(this);
            OnTransition = true;
        }

        public event BaseCanvasDelegate onShowFinished;
        protected virtual void OnShowFinished()
        {
            OnTransition = false;
            onShowFinished?.Invoke(this);
            //onStateChanged?.Invoke(this);
        }

        public event BaseCanvasDelegate onHideStart;
        protected virtual void OnHideStart()
        {
            onHideStart?.Invoke(this);
            OnTransition = true;
        }

        public event BaseCanvasDelegate onHideFinished;
        protected virtual void OnHideFinished()
        {
            OnTransition = false;
            onHideFinished?.Invoke(this);
        }

        public virtual void Show(bool ignoreTransistion = false) => SetEnable(true, ignoreTransistion);
        public virtual void Hide(bool ignoreTransistion = false) => SetEnable(false, ignoreTransistion);
        public CanvasTransferer TransferCanvas (BaseCanvas nextCanvas, bool ignoreTransition = false) => new CanvasTransferer(this, nextCanvas, ignoreTransition);
    }
}

