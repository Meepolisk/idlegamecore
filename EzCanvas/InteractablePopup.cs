﻿using System.Collections.Generic;
using UnityEngine;

namespace RTool.EzCanvas
{
    public class InteractablePopup : PopupCanvas
    {
        [Header("Component Ref")]
        [SerializeField, ReadOnly]
        private List<InteractableButton> interactableButtons = null;

        public delegate void OnPopupConfirm();
        internal Dictionary<string, OnPopupConfirm> onConfirmDict = new Dictionary<string, OnPopupConfirm>();
        internal void RegistConfirmDelegate(string key, OnPopupConfirm delegateAction)
        {
            if (onConfirmDict.ContainsKey(key))
                onConfirmDict[key] = delegateAction;
            else
                onConfirmDict.Add(key, delegateAction);
        }
        internal virtual void OnConfirmPopupExecuse(string keyExecused)
        {
            if (onConfirmDict.ContainsKey(keyExecused))
                onConfirmDict[keyExecused].Invoke();
            else
                Debug.LogWarningFormat(gameObject, "{0} trying to execuse key {1} but null", gameObject.name, keyExecused);
        }

        protected virtual void Awake()
        {
            if (interactableButtons != null && interactableButtons.Count > 0)
                interactableButtons.ForEach(button => button.Setup(this));
        }
    }
    public static class InteractablePopupExtensionMethods
    {
        /// <summary>
        /// Set react Action for this popup when being interacted by user
        /// </summary>
        /// <typeparam name="T">Type: <see cref="InteractablePopup"/> or derived from it</typeparam>
        /// <param name="interactablePopup">Object from <see cref="InteractablePopup"/>'s family member</param>
        /// <param name="interactKey">String key to catch</param>
        /// <param name="onPopupConfirm">Void function with no parameter, invoked when user interact with corresponding button key</param>
        /// <returns></returns>
        public static T onInteract<T>(this T interactablePopup, string interactKey, InteractablePopup.OnPopupConfirm onPopupConfirm) where T : InteractablePopup
        {
            interactablePopup.RegistConfirmDelegate(interactKey, onPopupConfirm);
            return interactablePopup;
        }
    }
}
