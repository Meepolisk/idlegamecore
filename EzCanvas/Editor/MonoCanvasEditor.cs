﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using REditor;

namespace RTool.EzCanvas
{
    public partial class MonoCanvas : BaseCanvas
    {
        [CustomEditor(typeof(MonoCanvas), true, isFallback = true)]
        private class MonoCanvasInspector : BaseCanvasInspector
        {
            private new MonoCanvas handler;
            protected override void OnEnable()
            {
                base.OnEnable();
                handler = base.Handler as MonoCanvas;
            }
            protected override void DrawActiveButton(Rect activeRect)
            {
                GUI.Toggle(activeRect, handler.m_Enable,
                   new GUIContent("Default", "Is this " + nameof(MonoCanvas) + " is default"), EditorStyles.miniButton)
                   .RecordValue(handler.m_Enable, value =>
                   {
                       //if (value)
                       //    handler.controller.EditorSetCurrentActiveCanvas(handler);
                       //else
                       //    handler.controller.EditorSetCurrentActiveCanvas(null);
                   });
            }

            protected override void PreviewButtonPressed(bool _value)
            {
                //if (handler != handler.controller.CurrentActiveCanvas)
                //{
                //    if (_value == true)
                //        handler.Controller.ForceVisible(handler);
                //    else
                //        handler.Controller.ResetAllPreview();
                //}
                //else
                //{
                //    base.PreviewButtonPressed(_value);
                //}
                base.PreviewButtonPressed(_value);
            }
        }
    }
}
#endif
