﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using REditor;
using UnityEditor;

namespace RTool.EzCanvas
{
    public partial class BaseCanvas : MonoBehaviour //Editor
    {
        #region object id
        protected static Dictionary<int, int> idList = new Dictionary<int, int>();
        protected static List<BaseCanvas> activeBaseCanvases = new List<BaseCanvas>();
        protected virtual void OnValidate()
        {
            if (gameObject.scene.name != null && idList.ContainsKey(GetInstanceID()) == false)
            {
                EditorAwake();
                idList.Add(GetInstanceID(), gameObject.GetInstanceID());
                activeBaseCanvases.Add(this);
                IsPreview = false;
                EditorApplication.hierarchyChanged += EditorApplication_hierarchyChanged;
            }
        }
        protected virtual void Reset()
        {
            EditorAwake();
        }
        protected virtual void EditorAwake()
        {
            AssignCanvas();
            UpdateEditorStats();
        }
        private void EditorApplication_hierarchyChanged()
        {
            if (this == null && idList.ContainsKey(GetInstanceID()))
            {
                EditorOnDestroy();
                EditorApplication.hierarchyChanged -= EditorApplication_hierarchyChanged;
            }
        }
        protected virtual void EditorOnDestroy()
        {
            GameObject go = EditorUtility.InstanceIDToObject(idList[GetInstanceID()]) as GameObject;
            if (go != null) //vẫn còn gameobject
            {
                go.GetComponent<CanvasGroup>().hideFlags = HideFlags.None;
                EditorUtility.SetDirty(go);
            }
            idList.Remove(GetInstanceID());
            activeBaseCanvases.Remove(this);
        }

        private void AssignCanvas()
        {
            if (canvasGroup == null)
            {
                canvasGroup = GetComponent<CanvasGroup>();
                if (canvasGroup == null)
                    canvasGroup = gameObject.AddComponent<CanvasGroup>();
            }
        }

        private bool m_Preview { get; set; }
        internal bool IsPreview
        {
            get => m_Preview;
            set
            {
                if (m_Preview == value)
                    return;

                m_Preview = value;
                if (m_Preview == true)
                {
                    if (Application.isPlaying == false)
                        EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
                }
                else
                {
                    if (Application.isPlaying == false)
                        EditorApplication.playModeStateChanged -= EditorApplication_playModeStateChanged;
                }
                UpdateEditorStats();
            }
        }
        private void EditorApplication_playModeStateChanged(PlayModeStateChange obj)
        {
            if (obj == PlayModeStateChange.ExitingEditMode)
                IsPreview = false;
        }
        #endregion

        private void UpdateEditorStats()
        {
            //Debug.Log("UpdateEditorStats");
            canvasGroup.alpha = m_Preview ^ m_Enable ? activeAlpha : 0f;
            RefreshBlocksRaycasts();
            RefreshInteractable();
        }
        [CustomEditor(typeof(BaseCanvas), true, isFallback = true)]
        internal class BaseCanvasInspector : UnityObjectEditor<BaseCanvas>
        {
            protected const float minAlpha = 0.05f;
            protected const float maxAlpha = 1f;
            protected const float previewButtonSize = 35f;
            const float alphaValueWidth = 40f;
            const float minAlphaRectGroup = 100f;
            const string enableDecr = "Is this " + nameof(BaseCanvas) + " enabled";
            const string alphaDecr = "Is this " + nameof(BaseCanvas) + " interactable (are the elements beneath the group enabled) when Enabled";
            const string interactDecr = "Is this " + nameof(BaseCanvas) + " interactable (are the elements beneath the group enabled) when Enabled";
            const string raycastDecr = "Does this " + nameof(BaseCanvas) + " block raycasting (allow collision) when Enabled";
            const string ignoreDecr = "Does this " + nameof(BaseCanvas) + " ignore setting from parent " + nameof(BaseCanvas);

            protected override void OnEnable()
            {
                base.OnEnable();
                if (!Application.isPlaying)
                    Handler.canvasGroup.hideFlags = HideFlags.HideInInspector;
                    //handler.canvasGroup.hideFlags = HideFlags.None;
                else
                    Handler.canvasGroup.hideFlags = HideFlags.None;
            }
            protected override void DrawingInspector()
            {
                EditorGUI.BeginChangeCheck();
                GUILayout.BeginVertical(EditorStyles.helpBox);
                DrawControlBox();
                GUILayout.EndVertical();
                if (EditorGUI.EndChangeCheck())
                {
                    Handler.UpdateEditorStats();
                }
            }
            
            private void SetCheck_IgnoreParent(bool ignore)
            {
                if (ignore == Handler.IgnoreParent)
                    return;
                Undo.RecordObject(Handler.canvasGroup, nameof(Handler.GetType) + " " + nameof(Handler.IgnoreParent));
                Handler.IgnoreParent = ignore;
            }

            private bool expand { get; set; }
            protected virtual void DrawControlBox()
            {
                DrawShortcutBox();
                if (expand)
                {
                    GUILayout.Label("Advanced Options", EditorStyles.centeredGreyMiniLabel);
                    DrawAdvancedBox();
                }
            }
            protected virtual void DrawShortcutBox()
            {
                Rect rect = EditorGUILayout.GetControlRect();
                Rect mainRect = new Rect(rect);
                mainRect.y -= 3f;
                mainRect.height += 7f;

                Rect previewRect = new Rect(mainRect.x - 15f, mainRect.y + 2, 25, mainRect.height - 5f);
                DrawPreviewButton(previewRect, EditorStyles.miniButton);

                Rect activeRect = new Rect(mainRect.x + 15f, mainRect.y, 75, mainRect.height);
                DrawActiveButton(activeRect);

                Rect alphaGroupRect = new Rect(activeRect.xMax + 3f, mainRect.y, mainRect.width - activeRect.xMax - previewButtonSize * 2 - 3f, mainRect.height);
                Rect alphaValueRect;
                float alpha;
                if (alphaGroupRect.width > minAlphaRectGroup)
                {
                    alphaValueRect = new Rect(alphaGroupRect.xMax - alphaValueWidth, rect.y, alphaValueWidth, rect.height);
                    Rect alphaSliderRect = new Rect(alphaGroupRect.x, rect.y, alphaGroupRect.width - alphaValueWidth - 5f, rect.height);
                    alpha = GUI.HorizontalSlider(alphaSliderRect, Handler.activeAlpha, minAlpha, maxAlpha);
                    alpha.RecordValue(ref Handler.activeAlpha, Handler, nameof(BaseCanvas) + " ActiveAlpha");
                }
                else
                    alphaValueRect = new Rect(alphaGroupRect.xMax - alphaValueWidth,rect.y, alphaValueWidth, rect.height);
                alpha = EditorGUI.FloatField(alphaValueRect, GUIContent.none, Handler.activeAlpha);
                alpha = Mathf.Clamp(alpha, minAlpha, maxAlpha);
                alpha.RecordValue(ref Handler.activeAlpha, Handler, nameof(BaseCanvas) + " ActiveAlpha");

                Rect interactableRect = new Rect(mainRect.x + mainRect.width - previewButtonSize * 2 - 15, mainRect.y, previewButtonSize, mainRect.height);
                GUI.Toggle(interactableRect, Handler.activeInteract, 
                    EditorGUIUtility.IconContent("HoloLensInputModule Icon", interactDecr),
                    EditorStyles.miniButtonLeft).RecordValue(ref Handler.activeInteract, Handler, nameof(BaseCanvas) + " ActiveInteractable");

                Rect blockRaycastRect = new Rect(mainRect.xMax - previewButtonSize - 15, mainRect.y, previewButtonSize, mainRect.height);
                GUI.Toggle(blockRaycastRect, Handler.activeRaycast,
                    EditorGUIUtility.IconContent("Physics2DRaycaster Icon", raycastDecr),
                    EditorStyles.miniButtonRight).RecordValue(ref Handler.activeRaycast, Handler, nameof(BaseCanvas) + " ActiveBlockRaycasts");

                Rect settingRect = new Rect(mainRect.xMax -12, rect.y, 40, mainRect.height);
                expand = GUI.Toggle(settingRect, expand,
                    EditorGUIUtility.IconContent("SettingsIcon"), EditorStyles.label);
            }
            protected virtual void DrawActiveButton(Rect activeRect)
                => GUI.Toggle(activeRect, Handler.m_Enable,
                new GUIContent("Enabled", enableDecr), EditorStyles.miniButton)
                .RecordValue(ref Handler.m_Enable, Handler, nameof(BaseCanvas) + " Enable");

            protected virtual void DrawAdvancedBox()
            {
                EditorGUILayout.Slider(new GUIContent("Active Alpha", alphaDecr), Handler.activeAlpha, minAlpha, maxAlpha)
                    .RecordValue(ref Handler.activeAlpha, Handler, nameof(BaseCanvas) + " ActiveAlpha");
                EditorGUILayout.Toggle(new GUIContent("Active Interactable", interactDecr), Handler.activeInteract)
                    .RecordValue(ref Handler.activeInteract, Handler, nameof(BaseCanvas) + "ActiveInteractable");
                EditorGUILayout.Toggle(new GUIContent("Active BlockRaycasts", raycastDecr), Handler.activeRaycast)
                    .RecordValue(ref Handler.activeRaycast, Handler, nameof(BaseCanvas) + "ActiveBlockRaycasts");
                EditorGUILayout.Toggle(new GUIContent(nameof(Handler.IgnoreParent), ignoreDecr), Handler.IgnoreParent)
                    .RecordValue(Handler.IgnoreParent, value =>
                    {
                        Undo.RecordObject(Handler.canvasGroup, nameof(CanvasGroup) + " Ignore parent");
                        Handler.IgnoreParent = value;
                    });
            }
            protected virtual void DrawPreviewButton(Rect rect, GUIStyle _guiStyle)
            {
                GUIContent guiContent;
                if (Handler.m_Enable)
                    guiContent = EditorGUIUtility.IconContent("animationvisibilitytoggleoff", "Temporary hide this canvas");
                else
                    guiContent = EditorGUIUtility.IconContent("animationvisibilitytoggleon", "Temporary show this canvas");
                var value = GUI.Toggle(rect, Handler.IsPreview, guiContent, _guiStyle);
                if (value != Handler.IsPreview)
                    PreviewButtonPressed(value);
            }
            protected virtual void PreviewButtonPressed(bool _value)
            {
                Handler.IsPreview = _value;
            }
        }

        #region static menu call
        internal bool EditorVisible
        {
            get => (canvasGroup.alpha > 0);
            set
            {
                IsPreview = value ^ Visible;
                UpdateEditorStats();
            }
        }

        const string nameSpace = "EzCanvas";
        const string hideFlagName = RTool.rootNameSpace + " / " + nameSpace + " / " + "Hide all BaseCanvas";
        [MenuItem(hideFlagName)]
        private static void HideAllBaseCanvas()
        {
            foreach (var item in activeBaseCanvases)
            {
                if (item != null)
                {
                    item.EditorVisible = false;
                }
            }
        }
        [MenuItem(hideFlagName, true)]
        private static bool HideAllBaseCanvasCondition()
        {
            foreach (var item in activeBaseCanvases)
            {
                if (item.EditorVisible == true)
                    return true;
            }
            return false;
        }
        const string previewFlagName = RTool.rootNameSpace + " / " + nameSpace + " / " + "Reset all PreviewFlag";
        [MenuItem(previewFlagName)]
        private static void ResetAllPreviewFlag()
        {
            foreach (var item in activeBaseCanvases)
            {
                if (item != null)
                    item.IsPreview = false;
            }
        }
        [MenuItem(previewFlagName, true)]
        private static bool ResetAllPreviewFlagCondition()
        {
            foreach (var item in activeBaseCanvases)
            {
                if (item.IsPreview == true)
                    return true;
            }
            return false;
        }
        [MenuItem(RTool.rootNameSpace + "/" + nameSpace + "/Check Instance IDs")]
        private static void CheckID()
        {
            Debug.Log("Current idList count: " + idList.Count);
            foreach (var item in idList)
            {
                string str = "id: " + item.Key;
                str += ", in object: " + item.Value;
                GameObject go = EditorUtility.InstanceIDToObject(item.Value) as GameObject;
                if (go != null && go.scene.name != null)
                {
                    Debug.Log(str, go);
                }
                else
                {
                    Debug.Log(str);
                }
            }
            Debug.Log("Current activeBaseCanvases count: " + activeBaseCanvases.Count);
            foreach (var item in activeBaseCanvases)
            {
                Debug.Log(item.name, item);
            }
        }
        private static T SafeCreateObject<T>(string _undoMsg, params Type[] _additionComponent) where T : Component
        {
            return SafeCreateObject<T>(_undoMsg, null, _additionComponent);
        }

        private static T SafeCreateObject<T>(string _undoMsg, Action<T> onCreate = null, params Type[] _additionComponent) where T : Component
        {
            if (string.IsNullOrEmpty(_undoMsg))
                _undoMsg = "Create " + typeof(T).Name;

            T newObject = FindObjectOfType<T>();
            if (newObject == null)
            {
                List<Type> alltype = new List<Type> {typeof(T)};
                if (_additionComponent != null && _additionComponent.Length > 0)
                    alltype.AddRange(_additionComponent);

                newObject = new GameObject(typeof(T).Name, alltype.ToArray()).GetComponent<T>();
                onCreate?.Invoke(newObject);
                Undo.RegisterCreatedObjectUndo(newObject.gameObject, _undoMsg);
            }
            return newObject;
        }
        private static Canvas SafeFindCanvas(string _undoMsg)
        {
            Canvas currentCanvas = SafeCreateObject<Canvas>(_undoMsg, typeof(UnityEngine.UI.CanvasScaler), typeof(UnityEngine.UI.GraphicRaycaster));
            currentCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
            SafeCreateObject<EventSystem>(_undoMsg, typeof(StandaloneInputModule));
            return currentCanvas;
        }

        [MenuItem("GameObject/" + nameSpace + "/BaseCanvas", false, priority = 104)]
        private static void CreateBaseCanvas()
        {
            string smg = "Create " + typeof(BaseCanvas).Name;
            Canvas currentCanvas = SafeFindCanvas(smg);
            
            BaseCanvas baseCanvas = new GameObject(typeof(BaseCanvas).Name).AddComponent<BaseCanvas>();
            Undo.RegisterCreatedObjectUndo(baseCanvas.gameObject, smg);
            baseCanvas.EditorSetupCreatedViaContextMenu(currentCanvas.transform);
            Selection.SetActiveObjectWithContext(baseCanvas.gameObject, baseCanvas);
        }
        //[MenuItem("GameObject/" + nameSpace + "/MonoCanvas", false)]
        //private static void CreateMonoCanvas()
        //{
        //    string smg = "Create " + typeof(MonoCanvas).Name;
        //    Canvas currentCanvas = SafeFindCanvas(smg);
        //    MonoCanvasesController currentController = SafeCreateObject<MonoCanvasesController>(smg,
        //    onCreate: (controller =>
        //    {
        //        controller.EditorSetupCreatedViaContextMenu(currentCanvas.transform);
        //        //check if there is popup bg, set sibling below that
        //        PopupBackground popupBG = FindObjectOfType<PopupBackground>();
        //        if (popupBG != null)
        //        {
        //            controller.transform.SetSiblingIndex(popupBG.transform.GetSiblingIndex());
        //        }
        //    }), typeof(RectTransform));

        //    MonoCanvas monoCanvas = new GameObject(typeof(MonoCanvas).Name).AddComponent<MonoCanvas>();
        //    Undo.RegisterCreatedObjectUndo(monoCanvas.gameObject, smg);
        //    //setup the controller
        //    monoCanvas.Controller = currentController;
        //    if (currentController.CurrentActiveCanvas == null)
        //    {
        //        currentController.EditorSetCurrentActiveCanvas(monoCanvas);
        //    }
        //    else
        //    {
        //        monoCanvas.Enable = false;
        //    }
        //    monoCanvas.EditorSetupCreatedViaContextMenu(currentController.transform);
        //    Selection.SetActiveObjectWithContext(monoCanvas.gameObject, monoCanvas);
        //}
        
        [MenuItem("GameObject/" + nameSpace + "/Popup", false)]
        private static void CreatePopup()
        {
            string smg = "Create " + typeof(PopupCanvas).Name;
            Canvas currentCanvas = SafeFindCanvas(smg);
            PopupBackground currentPopupBG = SafeCreateObject<PopupBackground>(smg,
            onCreate : (popup =>
            {
                popup.EditorSetupCreatedViaContextMenu(currentCanvas.transform);
                popup.transform.SetAsLastSibling();
            }), typeof(RectTransform));

            PopupCanvas newPopup = new GameObject(typeof(PopupCanvas).Name).AddComponent<PopupCanvas>();
            newPopup.EditorSetupCreatedViaContextMenu(currentPopupBG.transform);
            Selection.SetActiveObjectWithContext(newPopup.gameObject, newPopup);
        }
        #endregion
    }

    internal static class ExtensionMethods
    {
        internal static void EditorSetupCreatedViaContextMenu(this Component component, Transform rootCanvas)
        {
            RectTransform rect = component.GetComponent<RectTransform>();
            if (rect == null)
                rect = component.gameObject.AddComponent<RectTransform>();

            rect.SetParent(rootCanvas);
            rect.anchorMin = Vector2.zero; rect.anchorMax = Vector2.one;
            rect.sizeDelta = Vector2.zero; rect.anchoredPosition = Vector2.zero;
        }
    }
}
#endif

