﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RTool;
using RTool.Utils;

namespace RTool.EzCanvas
{
    public class PopupBackground : MonoBehaviour
    {
        [Header("Config")]
        [SerializeField, Range(0f, 1f), ReadOnly]
        private float fadeInDuration = 0.2f;
        public float FadeInDuration => fadeInDuration;
        [SerializeField, Range(0f, 1f), ReadOnly]
        private float fadeOutDuration = 0.2f;
        public float FadeOutDuration => fadeOutDuration;
        [SerializeField, Range(0f, 1f), ReadOnly]
        private float fullMaskAlpha = 0.5f;
        public float FullMaskAlpha => fullMaskAlpha;

        private PopupBackgroundMesh bgMesh { set; get; }
        public static PopupBackground AutoInstance { private set; get; }
        private List<PopupCanvas> popUpList = new List<PopupCanvas>();

        private void Awake()
        {
            AutoInstance = this;
            if (bgMesh == null)
            {
                var newGO = new GameObject("BgMesh");
                newGO.transform.parent = transform;
                bgMesh = newGO.AddComponent<PopupBackgroundMesh>();
                bgMesh.Setup(this);
#if UNITY_EDITOR
                bgMesh.gameObject.hideFlags = HideFlags.NotEditable;
#endif
            }
        }

        internal void RegistPopup(PopupCanvas _popupCanvas)
        {
            if (popUpList.Contains(_popupCanvas))
                popUpList.Remove(_popupCanvas);
            else
                _popupCanvas.onHideFinished += HideFinish;
            popUpList.Push(_popupCanvas);
            ActivateMesh(_popupCanvas);
        }
        private void HideFinish(BaseCanvas _popup)
        {
            _popup.onHideFinished -= HideFinish;
            PopupCanvas popup = (PopupCanvas)_popup;
            if (popup == popUpList.PeekLast())
            {
                popUpList.Pop();
                TurnOffActivePopup();
            }
            else if (popUpList.Contains(popup))
            {
                popUpList.Remove(popup);
                popup.UserHide();
            }
        }
        internal void BackgroundMeshClicked()
        {
            PopupCanvas canvas = popUpList.Pop();
            if (canvas != null)
            {
                canvas.UserHide();
                TurnOffActivePopup();
            }
        }
        private void TurnOffActivePopup()
        {
            if (popUpList.Count > 0)
            {
                ActivateMesh(popUpList.PeekLast());
            }
            else
                bgMesh.Show(false);
        }

        private void ActivateMesh(PopupCanvas _canvas)
        {
            if (_canvas.transform.parent != transform)
                _canvas.transform.parent = transform;
            bgMesh.transform.SetAsLastSibling();
            _canvas.transform.SetAsLastSibling();
            bgMesh.Show();
        }
    }
}
