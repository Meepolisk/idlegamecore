﻿using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RTool.EzCanvas
{
    public class PopupCanvas : BaseCanvas
    {
        public override void Show(bool ignoreTransition = false)
        {
            if (PopupBackground.AutoInstance == null)
                Debug.LogWarningFormat(gameObject, "{0}:{1} trying to show up, but cannot find any {2}", nameof(PopupCanvas), gameObject.name, nameof(PopupBackground));
            else
                PopupBackground.AutoInstance.RegistPopup(this);
            base.Show(ignoreTransition);
        }

        public delegate void OnPopupExit();
        internal OnPopupExit onExitDelegate { get; set; }

        internal void UserHide()
        {
            Hide();
        }
        protected override void OnHideFinished()
        {
            base.OnHideFinished();
            onExitDelegate?.Invoke();
        }

#if UNITY_EDITOR
        [CustomEditor(typeof(PopupCanvas), true, isFallback = true)]
        private class PopUpInspector : BaseCanvasInspector
        {
            protected override void DrawControlBox()
            {
                var bg = FindObjectOfType<PopupBackground>();
                if (bg == null)
                {
                    EditorGUILayout.HelpBox(
                       "Could found any instance of " + typeof(PopupBackground).Name + ". This " + typeof(PopupCanvas).Name + " will behave as normal "
                       + typeof(BaseCanvas).Name,
                       MessageType.Warning, true);
                }
                base.DrawControlBox();
            }
        }
#endif
    }

    public static class PopupCanvasExtensionMethods
    {
        /// <summary>
        /// Set react Action for this popup when being closed by user
        /// </summary>
        /// <typeparam name="T">Type: <see cref="PopupCanvas"/> or derived from it</typeparam>
        /// <param name="popup">Object from <see cref="PopupCanvas"/>'s family member</param>
        /// <param name="onPopupExitDelegate">Void function with no parameter, invoked when user close popup</param>
        /// <returns></returns>
        public static T onExit<T>(this T popup, PopupCanvas.OnPopupExit onPopupExitDelegate) where T : PopupCanvas
        {
            popup.onExitDelegate = onPopupExitDelegate;
            return popup;
        }
    }
}
