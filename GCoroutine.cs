﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace RTool.Utils
{
    public class GCoroutine : MonoBehaviour
    {
        private static GCoroutine _instance;
        internal static GCoroutine Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameObject(typeof(GCoroutine).Name).AddComponent<GCoroutine>();
                    DontDestroyOnLoad(_instance);
                }
                return _instance;
            }
        }

        public static Coroutine DelayCall(float time, Action action) => Instance.StartCoroutine(_DelayCall(time, action));
        internal static IEnumerator _DelayCall(float time, Action action)
        {
            yield return new WaitForSeconds(time);
            action?.Invoke();
        }

        //public static Coroutine UpdateCall(Func<bool> endingCondition, Action action) => Instance.StartCoroutine(_UpdateCall(time, action));
        //internal static IEnumerator _UpdateCall(float time, Action action)
        //{
        //    while (true)
        //    {
        //        yield return null;
        //        action.Invoke();
        //    }
        //}
    }
    public static class GCoroutineExtension
    {
        public static Coroutine StartCoroutine(this IEnumerator routine) => GCoroutine.Instance.StartCoroutine(routine);
        public static void Stop(this Coroutine coroutine) => GCoroutine.Instance.StopCoroutine(coroutine);
    }
}
