﻿using UnityEngine;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RTool.ScriptableJson
{
	public class JsonDataException : System.Exception
	{
		public JsonDataException(string fullPath)
			: base(string.Format("Invalid or corrupted data file at {0}", fullPath)) { }
	}

	public abstract class ScriptableJson : ScriptableObject
	{
#if UNITY_EDITOR
		const string extensionName = "txt";
		const bool prettyPrint = true;
#else
        const string extensionName = "data";
        const bool prettyPrint = false;
#endif
		[SerializeField, HideInInspector]
		private byte saveIntervalMinimum = 5;
		[SerializeField, HideInInspector]
		private string encryptKey = null;

		private float lastSaveTime = 0f;
		public virtual bool CanSaveData()
			=> saveIntervalMinimum == 0
			|| Time.unscaledTime - lastSaveTime > saveIntervalMinimum;

		private bool HaveEncryptKey()
			=> string.IsNullOrEmpty(encryptKey) == false;
		private string folderPath => Application.persistentDataPath;
		private string dataFilePath => string.Format("{0}/{1}.{2}", folderPath, name, extensionName);

		private void Save(string json)
		{
			Debug.LogFormat("<color=cyan>ScriptableJson</color>: saved to {0}\n{1}", dataFilePath, json);
			if (HaveEncryptKey())
				json = EncryptionHelper.Encrypt(json, encryptKey);
			File.WriteAllText(dataFilePath, json);
			lastSaveTime = Time.unscaledTime;
		}

		public void SaveJson(string json)
		{
			if (CanSaveData() == false)
				return;

			Save(json);
		}
		public void SaveRawData<T>(T data)
		{
			if (CanSaveData() == false)
				return;

			Save(ToJson(data));
		}

		public virtual bool CanLoadData()
			=> HasSaveInStorage();
		protected bool HasSaveInStorage()
			=> File.Exists(dataFilePath);

		private string Load()
		{
			string jsonData = File.ReadAllText(dataFilePath);
			if (HaveEncryptKey())
				jsonData = EncryptionHelper.Decrypt(jsonData, encryptKey);
			Debug.LogFormat(this, "<color=cyan>[ScriptableJson]{0}</color>: loaded at {1}/n{2}", name, dataFilePath, jsonData);
			return jsonData;
		}

		public string LoadJson()
		{
			if (CanLoadData() == false)
				throw new JsonDataException(dataFilePath);

			return Load();
		}
		public T LoadRawData<T>()
		{
			if (CanLoadData() == false)
				throw new JsonDataException(dataFilePath);

			string json = Load();
			if (string.IsNullOrEmpty(json))
				throw new JsonDataException(dataFilePath);
			return FromJson<T>(json);
		}

		public T LoadRawData<T>(string json)
		{
			if (string.IsNullOrEmpty(json))
				throw new System.Exception("null json");
			return FromJson<T>(json);
		}

		public void DeleteRawData()
		{
			if (HasSaveInStorage())
				File.Delete(dataFilePath);
		}

		protected virtual string ToJson<T>(T data)
			=> JsonUtility.ToJson(data, prettyPrint);
		protected virtual T FromJson<T>(string json)
			=> JsonUtility.FromJson<T>(json);

		public abstract void SaveData();
		public abstract void LoadData();

#if UNITY_EDITOR
		[CustomEditor(typeof(ScriptableJson), true, isFallback = true)]
		private class CustomInspector : Editor
		{
			ScriptableJson handler = null;

			private void OnEnable()
			{
				handler = (ScriptableJson)target;
			}

			public override void OnInspectorGUI()
			{
				SerializedProperty prop = serializedObject.FindProperty("m_Script");
				GUI.enabled = false;
				EditorGUILayout.PropertyField(prop, true, new GUILayoutOption[0]);
				GUI.enabled = true;

				EditorGUILayout.BeginVertical(EditorStyles.helpBox);
				handler.saveIntervalMinimum = (byte)EditorGUILayout.IntField(new GUIContent(
					"Saving Interval Minimum", "Minimum second between save"), handler.saveIntervalMinimum);
				handler.encryptKey = EditorGUILayout.TextField(new GUIContent(
					"Encrypt Key", "Custom key to encrypt the file. Leave blank for no encyption"), handler.encryptKey);
				EditorGUILayout.BeginHorizontal();
				if (GUILayout.Button("Save"))
				{
					handler.SaveData();
					EditorUtility.RevealInFinder(handler.dataFilePath);
				}
				GUI.enabled = handler.CanLoadData();
				if (GUILayout.Button("Load"))
				{
					handler.LoadData();
				}
				if (GUILayout.Button("OpenFolder"))
				{
					EditorUtility.RevealInFinder(handler.dataFilePath);
				}
				if (GUILayout.Button("Delete"))
				{
					handler.DeleteRawData();
				}
				GUI.enabled = true;
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.EndVertical();

				DrawPropertiesExcluding(serializedObject, "m_Script");
				serializedObject.ApplyModifiedProperties();
			}
		}

		//todo: add context menu: Clear All Data
#endif
	}

	public abstract class ScriptableJson<T> : ScriptableJson
	{
		[SerializeField]
		protected T data = default;
		public T Data => data;

		public sealed override void SaveData()
		{
			if (CanSaveData())
			{
				SaveRawData(data);
			}
		}

		public void SaveData(T newData)
		{
			data = newData;
			SaveData();
		}

		public sealed override void LoadData()
		{
			if (HasSaveInStorage())
			{
				data = LoadRawData<T>();
				return;
			}
			AssigneDefaultData();
		}

		public void LoadData(string json)
		{
			if (!string.IsNullOrEmpty(json))
			{
				data = LoadRawData<T>(json);
				return;
			}
			AssigneDefaultData();
		}

		public string SerializeData()
		{
			return ToJson(data);
		}

		public void ResetData(bool deleteCurrentData = true)
		{
			if (deleteCurrentData)
				DeleteRawData();
			AssigneDefaultData();
		}
		protected abstract void AssigneDefaultData();

		protected virtual void OnEnable()
		{
			Application.quitting += Application_quitting;
		}

		private void Application_quitting()
		{
			SaveRawData(data);
		}
	}
}