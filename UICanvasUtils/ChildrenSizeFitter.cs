﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RTool.UICanvasUtil
{
    [ExecuteInEditMode]
    [AddComponentMenu("Layout/ChildrenSizeFitter")]
    [RequireComponent(typeof(RectTransform))]
    public class ChildrenSizeFitter : UIBehaviour
    {
        [SerializeField]
        private float offsetTop = 0;
        [SerializeField]
        private float offsetBottom = 0;
        [SerializeField]
        private float spacing = 0;

        List<RectTransform> listChildren = new List<RectTransform>();
        
        private void OnTransformChildrenChanged()
        {
            GetChildren();
        }
        private void RefreshDaTransform()
        {
            RectTransform rect = transform as RectTransform;
            //ApplyPivot(rect);
            //ApplyAnchor(rect);
            rect.ForceUpdateRectTransforms();

            float currentY = offsetTop;
            for (int i = 0; i < listChildren.Count; i++)
            {
                var child = listChildren[i];
                ApplyPivot(child);
                ApplyAnchor(child);
                child.ForceUpdateRectTransforms();

                Vector2 ancPos = child.anchoredPosition;
                ancPos.y = -currentY;
                child.anchoredPosition = ancPos;

                currentY += child.sizeDelta.y;
                if (i != listChildren.Count - 1)
                    currentY += spacing;
            }
            Vector2 sizeDelta = rect.sizeDelta;
            sizeDelta.y = currentY + offsetBottom;
            rect.sizeDelta = sizeDelta;
            //Debug.LogFormat("RefreshUI: {0} childs", listChildren.Count);
            rect.ForceUpdateRectTransforms();
        }
        private void ApplyPivot(RectTransform rect)
        {
            Vector2 childPivot = rect.pivot;
            childPivot.y = 1f;
            rect.pivot = childPivot;
        }
        private void ApplyAnchor(RectTransform rect)
        {
            Vector2 childPivot = rect.anchorMax;
            childPivot.y = 1f;
            rect.anchorMax = childPivot;
        }
        protected override void Start()
        {
            base.Start();
            GetChildren();
        }
        private void Update()
        {
            if (listChildren.Count > 0)
            {
                foreach(var transform in listChildren)
                {
                    if (transform.hasChanged)
                    {
                        GetChildren();
                        return;
                    }
                }
            }
        }
#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
            GetChildren();
        }
#endif
        private void GetChildren()
        {
            listChildren.Clear();
            for (int i = 0; i < transform.childCount; i++)
            {
                RectTransform rect = transform.GetChild(i).GetComponent<RectTransform>();
                if (rect != null && rect.gameObject.activeInHierarchy)
                    listChildren.Add(rect);
            }
            RefreshDaTransform();
        }
    }
}
