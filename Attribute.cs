﻿using System;
using System.Linq.Expressions;
using UnityEngine;

namespace RTool
{
    public class EditScriptableAttribute : PropertyAttribute { }

    public class ReorderableAttribute : PropertyAttribute
    {
        public string ElementHeader { get; protected set; }
        public bool HeaderZeroIndex { get; protected set; }
        public bool ElementSingleLine { get; protected set; }

        public ReorderableAttribute()
        {
            ElementHeader = string.Empty;
            HeaderZeroIndex = false;
            ElementSingleLine = false;
        }

        public ReorderableAttribute(string headerString = "", bool isZeroIndex = true, bool isSingleLine = false)
        {
            ElementHeader = headerString;
            HeaderZeroIndex = isZeroIndex;
            ElementSingleLine = isSingleLine;
        }
    }

    /// <summary>
    ///   <para>Attribute used to prevent user from changing value from unity inspector view.</para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class DebugAttribute : PropertyAttribute { }

    /// <summary>
    ///   <para>Only allow change in inspector during Unity Edit-mode / Prefab-edit-mode</para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ReadOnlyAttribute : PropertyAttribute
    {
        public readonly bool prefabMode;
        /// <summary>
        ///   <para>Attribute used to prevent user from changing value from unity inspector view during play session</para>
        /// </summary>
        /// <param name="prefabMode">Will the attribute prevent edit this field in animator-mode too</param>
        public ReadOnlyAttribute(bool prefabMode)
        {
            this.prefabMode = prefabMode;
        }
        /// <summary>
        ///   <para>Attribute used to prevent user from changing value from unity inspector view during play session</para>
        /// </summary>
        public ReadOnlyAttribute()
        {
            prefabMode = false;
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class RequireAttribute : PropertyAttribute { }
    
}