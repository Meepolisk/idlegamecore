﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace REditor
{
    public abstract class UnityObjectEditor<T> : Editor where T : Object
    {
		public T Handler { private set; get; }

        protected bool ignoreBase = false;

        protected virtual void OnEnable() => Handler = target as T;

        public sealed override void OnInspectorGUI()
        {
            serializedObject.Update();
			Rect rect = EditorGUILayout.GetControlRect();
			var rightClickMenu = rect.CreateRightClickMenu();
			if (rightClickMenu != null)
			{
				rightClickMenu.AddItem(new GUIContent("Force Save"), false, ForceSave);
				rightClickMenu.ShowAsContext();
			}
			SerializedProperty prop = serializedObject.FindProperty("m_Script");
            GUI.enabled = false;
            EditorGUI.PropertyField(rect, prop);
			GUI.enabled = true;

			var drawCondition = CustomDrawCondition();
			if (drawCondition.value == true)
			{
				DrawingInspector();
				if (ignoreBase == false)
					DrawPropertiesExcluding(serializedObject, "m_Script");
			}
			else
			{
				EditorGUILayout.HelpBox(drawCondition.message, MessageType.Error);
				DrawPropertiesExcluding(serializedObject, "m_Script");
			}
            serializedObject.ApplyModifiedProperties();
        }

		protected virtual (bool value, string message) CustomDrawCondition()
			=> (true, "");

        protected abstract void DrawingInspector();

		private void ForceSave()
		{
			EditorUtility.SetDirty(Handler);
			AssetDatabase.SaveAssets();
		}
	}

	public abstract class UnityPagingEditor<T> : UnityObjectEditor<T> where T : Object
	{
		private List<IPaging> pagingList = null;
		private int selectedPage = 0;

		protected override void OnEnable()
		{
			base.OnEnable();
			pagingList = SetupPaging;
			pagingList[selectedPage].OnEnable();
		}
		protected abstract List<IPaging> SetupPaging { get; }

		protected sealed override void DrawingInspector()
		{
			int newPage = selectedPage;
			GUILayout.BeginHorizontal();
			for(int i = 0; i < pagingList.Count; i++)
			{
				if (GUILayout.Toggle(newPage == i, new GUIContent(pagingList[i].Label), MiniButtonStyleIndex(i, pagingList.Count)) == true)
					newPage = i;
			}
			GUILayout.EndVertical();
			if (newPage != selectedPage)
			{
				pagingList[selectedPage].OnDisable();
				pagingList[newPage].OnEnable();
				selectedPage = newPage;
			}

			if (selectedPage != -1)
			{
				pagingList[selectedPage].OnDrawing();
			}
			ignoreBase = true;
		}

		private GUIStyle MiniButtonStyleIndex (int index, int maxIndex)
		{
			if (index == 0)
				return EditorStyles.miniButtonLeft;
			if (index == maxIndex - 1)
				return EditorStyles.miniButtonRight;
			return EditorStyles.miniButton;
		}

		protected override (bool, string) CustomDrawCondition()
		{
			if (pagingList == null || pagingList.Count <= 1)
				return (false, string.Format("{0} must have atleast 2 member", nameof(pagingList)));
			return base.CustomDrawCondition();
		}
		public struct Page : IPaging
		{
			private readonly GUIContent label;
			private readonly System.Action onEnabledAction;
			private readonly System.Action onDisabledAction;
			private readonly System.Action onDrawingAction;

			public GUIContent Label
				=> label;
			public void OnEnable()
				=> onEnabledAction?.Invoke();
			public void OnDisable()
				=> onDisabledAction?.Invoke();
			public void OnDrawing()
				=> onDrawingAction?.Invoke();

			public Page(GUIContent guiContent, System.Action enable, System.Action disable, System.Action drawing)
			{
				label = guiContent;
				onEnabledAction = enable;
				onDisabledAction = disable;
				onDrawingAction = drawing;
			}
			public Page(string label, System.Action enable, System.Action disable, System.Action drawing)
			{
				this.label = new GUIContent(label);
				onEnabledAction = enable;
				onDisabledAction = disable;
				onDrawingAction = drawing;
			}
		}

		protected interface IPaging
		{
			GUIContent Label { get; }
			void OnEnable();
			void OnDisable();
			void OnDrawing();
		}
	}

	public abstract class UnityAdvancedEditor<T> : UnityObjectEditor<T> where T : Object
	{
		protected class ControlManager
		{
			public string ActiveControl { get; private set; }
			public string UnfocusedControl { get; private set; }
			public string FocusedControl { get; private set; }

			public ControlManager()
			{
				ActiveControl = null;
				UnfocusedControl = null;
				FocusedControl = null;
			}
			public void UpdateFocusedControl()
			{
				FocusedControl = null;
				UnfocusedControl = null;
				string checkingControl = GUI.GetNameOfFocusedControl();
				if (checkingControl != ActiveControl)
				{
					if (UnfocusedControl == null)
						UnfocusedControl = ActiveControl;
					if (FocusedControl == null)
						FocusedControl = checkingControl;
					ActiveControl = checkingControl;
				}
			}
		}
		protected ControlManager controlManager { private set ; get; }

		protected override void OnEnable()
		{
			base.OnEnable();
			controlManager = new ControlManager();
		}

		protected sealed override void DrawingInspector()
		{
			controlManager.UpdateFocusedControl();
			DrawingInspector(controlManager);
		}
		protected abstract void DrawingInspector(ControlManager controlManager);
	}
}
#endif