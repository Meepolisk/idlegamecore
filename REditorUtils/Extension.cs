﻿#if UNITY_EDITOR
using UnityEngine;
using UObject = UnityEngine.Object;
using UnityEditor;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using JetBrains.Annotations;

namespace REditor
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Check value and record to editor. Support Undo action
        /// </summary>
        /// <typeparam name="T">Equalable Type</typeparam>
        /// <param name="value">Value want to inject, ignore Ref for property</param>
        /// <param name="tRef">Ref param inject to</param>
        /// <param name="recordUndoObject">Unity Object that store the Ref Param</param>
        /// <param name="message">Message for Undo</param>
        /// <returns></returns>
        public static bool RecordValue<T> (this T value, ref T tRef, [NotNull] UObject recordUndoObject, [NotNull] string message)
        {
            if (value.Equals(tRef))
                return false;
            
            Undo.RecordObject(recordUndoObject, message);
            tRef = value;
            return true;
        }
        /// <summary>
        /// Check value and record to editor. Support Undo action
        /// </summary>
        /// <typeparam name="T">Equalable Type</typeparam>
        /// <param name="value">Value want to inject</param>
        /// <param name="tRef">Ref param inject to</param>
        /// <returns></returns>
        public static bool RecordValue<T>(this T value, ref T tRef)
        {
            if (value.Equals(tRef))
                return false;

            tRef = value;
            return true;
        }
        /// <summary>
        /// Check value and compare. If value is differnce, invoke action
        /// </summary>
        /// <typeparam name="T">Equalable Type</typeparam>
        /// <param name="value">Value want to inject, pass Ref for field</param>
        /// <param name="compared">Value use to compare"</param>
        /// <param name="actionReturn">Action return when new value detected</param>
        /// <returns></returns>
        public static bool RecordValue<T>(this T value, T compared, [NotNull] Action<T> actionReturn)
        {
            if (value.Equals(compared))
                return false;

            actionReturn.Invoke(value);
            return true;
        }

        public static GenericMenu CreateRightClickMenu(this Rect clickArea)
        {
            Event current = Event.current;
            if (clickArea.Contains(current.mousePosition) && current.type == EventType.ContextClick)
            {
                GenericMenu menu = new GenericMenu();
                current.Use();
                return menu;
            }
            return null;
        }
    }
}
#endif