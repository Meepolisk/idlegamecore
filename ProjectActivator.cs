﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RTool.ProjectActivator
{
    internal static class ProjectActivator
    {
        const string sceneCountKey = null;
        const string sceneKey = null;

        static bool isWarned = false;
        [UnityEditor.Callbacks.PostProcessScene]
        static void ProjectActivatorPostCallback()
        {
            int sceneSavedCount = EditorPrefs.GetInt(sceneCountKey);
            if (sceneSavedCount == 0)
                return;

            if (isWarned)
            {
                Debug.LogFormat("<color=blue>ProjectActivator-mode: {0} scene(s) is in edit</color>", sceneSavedCount);
                isWarned = true;
            }
            EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
        }

        [MenuItem("RTool/Launch Project _F5")]
        public static void PlayEditorScene0()
        {
            if (SceneManager.sceneCount == 1 
                && EditorBuildSettings.scenes[0].path == SceneManager.GetActiveScene().path)
            {
                EditorApplication.isPlaying = true;
            }
            else if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                SavePref();
                EditorSceneManager.OpenScene(EditorBuildSettings.scenes[0].path);
                EditorApplication.isPlaying = true;
            }
        }

        private static void EditorApplication_playModeStateChanged(PlayModeStateChange stateModeChange)
        {
            if (stateModeChange == PlayModeStateChange.EnteredEditMode)
            {
                string[] currentScenePath = LoadPref();
                if (currentScenePath != null && currentScenePath.Length > 0)
                {
                    if (EditorUtility.DisplayDialog("Re-open scene",
                        "Do you want to re-open your scene(s) before ProjectActivator-mode",
                        "Purrfect", "Nahh"))
                    {
                        EditorSceneManager.OpenScene(currentScenePath[0]);
                        if (currentScenePath.Length > 1)
                        {
                            for (int i = 1; i < currentScenePath.Length; i++)
                            {
                                EditorSceneManager.OpenScene(currentScenePath[i], OpenSceneMode.Additive);
                            }
                        }
                    }
                }
                DeletePref();
                EditorApplication.playModeStateChanged -= EditorApplication_playModeStateChanged;
            }
        }

        static string[] LoadPref()
        {
            int sceneCount = EditorPrefs.GetInt(sceneCountKey);
            string[] result = new string[sceneCount];
            for (int i = 0; i < sceneCount; i++)
            {
                result[i] = EditorPrefs.GetString(sceneKey + i);
            }
            return result;
        }

        static void SavePref()
        {
            int sceneCount = SceneManager.sceneCount;
            EditorPrefs.SetInt(sceneCountKey, sceneCount);
            for (int i = 0; i < sceneCount; i++)
            {
                EditorPrefs.SetString(sceneKey + i, SceneManager.GetSceneAt(i).path);
            }
        }
        
        static void DeletePref()
        {
            int count = EditorPrefs.GetInt(sceneCountKey);
            for (int i = 0; i< count; i++)
            {
                EditorPrefs.DeleteKey(sceneKey + i);
            }
            EditorPrefs.DeleteKey(sceneCountKey);
        }

        [MenuItem("RTool/Launch Project _F5", validate = true)]
        public static bool PlayEditorScene0Condition()
            => !EditorApplication.isPlaying && EditorBuildSettings.scenes.Length > 0;
    }
}
#endif