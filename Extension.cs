using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using System.Linq;

namespace RTool.Utils
{
	public static class ExtensionMethods
	{
		public static void Push<T>(this List<T> _list, T _object)
		{
			_list.Add(_object);
		}
		public static T PeekLast<T>(this List<T> _list) where T : class
		{
			if (_list.Count == 0)
				return null;

			return _list[_list.Count - 1];
		}
		public static T PeekFirst<T>(this List<T> _list) where T : class
		{
			if (_list.Count == 0)
				return null;

			return _list[0];
		}
		public static T Pop<T>(this List<T> _list) where T : class
		{
			if (_list.Count == 0)
				return null;
			var index = _list.Count - 1;
			var removedOne = _list[index];
			_list.RemoveAt(index);

			return removedOne;
		}

		public static T GetRandom<T>(this List<T> _list)
			=> _list[UnityEngine.Random.Range(0, _list.Count)];

		public static void ChangeX(this ref Vector3 vector, float value) => vector.x = value;
		public static void ChangeY(this ref Vector3 vector, float value) => vector.y = value;
		public static void ChangeZ(this ref Vector3 vector, float value) => vector.z = value;

		public static void ChangeX(this ref Vector2 vector, float value) => vector.x = value;
		public static void ChangeY(this ref Vector2 vector, float value) => vector.y = value;

		public static void LookAt2D(this Transform transform, Vector2 target)
		{
			Debug.DrawLine(transform.position, target, Color.red, 2f);
			Debug.DrawRay(transform.position, Vector2.right, Color.cyan, 2f);
			Vector2 direction = (target - (Vector2)transform.position).normalized;
			float angle = direction.DirectionTo2DAngle();
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, angle);
		}
		public static float DirectionTo2DAngle(this Vector2 direction)
			=> DirectionTo2DAngle(direction, Vector2.right);
		public static float DirectionTo2DAngle(this Vector2 direction, Vector2 compareVec)
			=> Vector2.SignedAngle(direction, compareVec) * -1f;

		public static List<T> Clone<T>(this List<T> _list) where T : ICloneable
		{
			return _list.Select(x => (T)x.Clone()).ToList();
		}
	}
}