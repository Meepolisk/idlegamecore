﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEditor;
namespace RTool
{
    public partial class RTool
    {
        internal const string rootNameSpace = "RTool";

        //todo: chỗ này làm List<Action> để reg / unreg action như kiểu SetHideFlag cho PopupCanvasMesh

        //internal static bool IsDebug = false;

        //private const string menuNameOn = rootNameSpace + " / " + "Debug ON";
        //[MenuItem(menuNameOn)]
        //private static void DebugOn()
        //{
        //    Debug.Log("RTool debug set to [ON]");
        //    IsDebug = true;
        //}
        //[MenuItem(menuNameOn, true)]
        //private static bool CanDebugOn() => IsDebug == false;

        //private const string menuNameOff = rootNameSpace + " / " + "Debug OFF";
        //[MenuItem(menuNameOff)]
        //private static void DebugOff()
        //{
        //    Debug.Log("RTool debug set to [OFF]");
        //    IsDebug = false;
        //}
        //[MenuItem(menuNameOff, true)]
        //private static bool CanDebugOFF() => IsDebug == true;

        [MenuItem("Assets/Load Additive Scene")]
        private static void LoadAdditiveScene()
        {
            var selected = Selection.activeObject;
            EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(selected));
        }

        [MenuItem("RTool/Clear HideFlags", validate = true)]
        private static bool ClearAllHideFlagCondition()
            => Selection.gameObjects != null && Selection.gameObjects.Length > 0;

        [MenuItem("RTool/Clear HideFlags")]
        private static void ClearAllHideFlag()
        {
            foreach(var gameObject in Selection.gameObjects)
            {
                Component[] components = gameObject.GetComponentsInChildren<Component>();
                foreach (var component in components)
                {
                    component.hideFlags = HideFlags.None;
                }
            }
        }
    }
}
#endif