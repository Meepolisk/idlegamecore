﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

namespace RTool.EzPooling
{
    public static class EzPooling
    {
        #region Cleaner
        static bool cleanerSetup = false;
        static void SignupAutoCleaner()
        {
            if (cleanerSetup)
                return;

            cleanerSetup = true;
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        }
        private static void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            CleanUp();
        }
        #endregion

        private readonly static Dictionary<Component, BaseCollection> collectionDict = new Dictionary<Component, BaseCollection>();
        private readonly static Dictionary<Component, BaseCollection> activePair = new Dictionary<Component, BaseCollection>();
        
        public static void ClearInactive()
        {
            foreach (var collection in collectionDict)
            {
                collection.Value.ClearInactive();
            }
        }
        public static void CleanUp()
        {
            foreach (var component in activePair)
            {
                if (component.Key == null)
                    activePair.Remove(component.Key);
            }

            //delete unused collection
            List<Component> toRemove = collectionDict.Where(pair => pair.Value.IsEmpty)
                         .Select(pair => pair.Key)
                         .ToList();

            foreach (var key in toRemove)
                collectionDict.Remove(key);
        }
        private static BaseCollection GetCollectionControl(Component obj)
            => activePair.ContainsKey(obj)
                ? activePair[obj] : null;
        private static Collection<T> GetCollection<T>(T prefab) where T : Component
            => collectionDict.ContainsKey(prefab)
                ? collectionDict[prefab] as Collection<T> : null;

        private static Collection<T> CreateCollection<T> (T prefab) where T : Component
            => new Collection<T>(prefab);

        public static T Instantiate<T> (T prefab) where T : Component
        {
            if (string.IsNullOrEmpty(prefab.gameObject.scene.name) == false)
            {
                return Object.Instantiate(prefab);
            }

            Collection<T> pool = GetCollection(prefab);
            if (pool == null)
                pool = CreateCollection(prefab);

            T newObject = pool.Instantiate();
            activePair.Add(newObject, pool);
            return newObject;
        }

        public static void Destroy<T>(T obj) where T : Component
            => Destroy(obj, 0f);
        public static void Destroy<T> (T obj, float delay) where T : Component
        {
            BaseCollection collection = GetCollectionControl(obj);
            if (collection == null)
            {
                Object.Destroy(obj.gameObject, delay);
                Debug.LogWarningFormat(obj.gameObject, "{0} try to Pooling Destroy but failed", obj.name);
                return;
            }
            collection.Destroy(obj, delay);
            activePair.Remove(obj);
        }

        private abstract class BaseCollection
        {
            public abstract void ClearInactive();
            public abstract void CleanUp();
            public abstract void Destroy(Component component, float delay);
            public abstract bool IsEmpty { get; }
        }

        private class Collection<T> : BaseCollection where T : Component
        {
            private readonly HashSet<T> activeHash = new HashSet<T>();
            private readonly Queue<T> inactiveQueue = new Queue<T>();

            private readonly T prefab;

            public Collection(T prefab)
            {
                if (string.IsNullOrEmpty(prefab.gameObject.scene.name) == false)
                    throw new System.Exception(string.Format("Original Object for {0} cannot active in scene", nameof(EzPooling)));

                SignupAutoCleaner();

                this.prefab = prefab;
                collectionDict.Add(prefab, this);
            }
            ~Collection()
            {
                if (collectionDict.ContainsKey(prefab))
                    collectionDict.Remove(prefab);
            }

            public override bool IsEmpty
                => activeHash.Count == 0 && inactiveQueue.Count == 0;

            private T CheckDequeueOrSpawnNew()
            {
                if (inactiveQueue.Count == 0)
                    return Object.Instantiate(prefab);

                T obj = inactiveQueue.Dequeue();
                if (obj == null)
                    return CheckDequeueOrSpawnNew();
                return obj;
            }
            private T SpawnOrGetAvailable()
            {
                T newObject = CheckDequeueOrSpawnNew();

                newObject.gameObject.SetActive(true);
                activeHash.Add(newObject);
                return newObject;
            }

            public T Instantiate()
            {
                T newObject = SpawnOrGetAvailable();
                return newObject;
            }

            public void Destroy(T poolingObject)
                => Destroy(poolingObject, 0f);
            public void Destroy(T poolingObject, float delay)
            {
                if (activeHash.Contains(poolingObject) == false)
                {
                    Object.Destroy(poolingObject.gameObject, delay);
                    return;
                }
                activeHash.Remove(poolingObject);
                poolingObject.gameObject.SetActive(false);
                inactiveQueue.Enqueue(poolingObject);
            }

            public sealed override void Destroy(Component component, float delay)
                => Destroy(component as T, delay);

            public sealed override void ClearInactive()
            {
                foreach (var item in inactiveQueue)
                {
                    if (activePair.ContainsKey(item))
                        activePair.Remove(item);
                    Object.Destroy(item.gameObject);
                }

                inactiveQueue.Clear();
            }

            public sealed override void CleanUp()
            {
                activeHash.RemoveWhere(obj => obj == null);
            }
        }
    }
}
