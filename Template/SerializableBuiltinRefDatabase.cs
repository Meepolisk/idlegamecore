﻿using UnityEngine;
using System.Collections.Generic;
using RTool;
using System;

namespace RTemplate.SerializableBuiltinRef
{
	/// <summary>
	/// Must call <see cref="Initialize"/> 1 time to work
	/// </summary>
	public abstract class BaseInventory<T, G> where T : BaseBSObject<G> where G : BaseBSData
	{
		protected abstract IEnumerable<G> Data { get; }
		public abstract IBSDatabase<T, G> Database { get; }

		protected List<T> inventory = new List<T>();
		public IReadOnlyList<T> Inventory => inventory;
		
		public bool IsInitialized { get; private set; }
		
		public void Initialize()
		{
			if (IsInitialized == true)
				return;

			OnInitialized();
			IsInitialized = true;
		}
		protected virtual void OnInitialized()
		{
			foreach (var itemData in Data)
			{
				CreateNewItem(itemData);
			}
		}

		private void CreateNewItem(G data)
		{
			var newItem = BaseBSObject<G>.CreateInstance(Database, data);
			inventory.Add(newItem);
		}

		protected virtual bool AddCondition(G itemData)
			=> itemData.databaseIndex >= 0 && itemData.databaseIndex < Database.PresetData.Count;
		public bool AddItem(G itemData)
		{
			if (AddCondition(itemData) == false)
				return false;

			OnAddItem(itemData);
			return true;
		}
		protected virtual void OnAddItem(G itemData)
		{
			T newItem = BaseBSObject<G>.CreateInstance(Database, itemData);
			inventory.Add(newItem);
			OnItemAdded?.Invoke(newItem);
		}

		protected virtual bool RemoveCondition(T item)
			=> inventory.Contains(item) == true;
		public bool RemoveItem(T item)
		{
			if (RemoveCondition(item) == false)
				return false;

			OnRemoveItem(item);
			return true;
		}

		protected virtual void OnRemoveItem(T item)
		{
			inventory.Remove(item);
			OnItemRemoved?.Invoke(item);
			UnityEngine.Object.Destroy(item);
		}

		public delegate void OnAddItemDelegate(T item);
		public static event OnAddItemDelegate OnItemAdded;
		public static event OnAddItemDelegate OnItemRemoved;
	}
}