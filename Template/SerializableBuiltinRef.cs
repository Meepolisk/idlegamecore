﻿using UnityEngine;
using System.Collections.Generic;

namespace RTemplate.SerializableBuiltinRef
{
	[SerializeField]
	public abstract class BaseBSData
	{
		[SerializeField]
		protected internal int databaseIndex = 0;
		public int DatabaseIndex => databaseIndex;
	}
	public abstract class BaseBSObject<G> : MonoBehaviour where G : BaseBSData
	{
		[SerializeField]
		G data = null;
		public G Data => data;

		public static T CreateInstance<T>(IBSDatabase<T,G> itemDatabase, G data) where T : BaseBSObject<G>
		{
			T newInstance = Instantiate(itemDatabase.PresetData[data.DatabaseIndex]);
			newInstance.data = data;
			return newInstance;
		}
		public static T CreateInstance<T>(IBSDatabase<T, G> itemDatabase, int index) where T : BaseBSObject<G>
		{
			T newInstance = Instantiate(itemDatabase.PresetData[index]);
			newInstance.data.databaseIndex = index;
			return newInstance;
		}
	}

	public interface IBSDatabase<T,G> where T : BaseBSObject<G> where G : BaseBSData
	{
		IReadOnlyList<T> PresetData { get; }
	}

	public static class ItemDatabaseExtensionMethod
	{
		public static T CreateInstance<T,G>(this IBSDatabase<T,G> itemDatabase, G data) where T : BaseBSObject<G> where G : BaseBSData
			=> BaseBSObject<G>.CreateInstance(itemDatabase, data);
	}
}
